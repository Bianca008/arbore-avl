#include<iostream>
#include<vector>
#include<queue>

struct NOD {
	int info, balansare;
	NOD *st, *dr, *p;
	NOD(const int val = 0) {
		info = val;
		balansare = 0;
		st = nullptr;
		dr = nullptr;
		p = nullptr;
	}
};

struct AVL {
	NOD *rad;
	AVL() {
		rad = nullptr;
	}
	NOD* avl_caut(int);
	NOD* avl_min(NOD*);
	NOD* avl_max(NOD*);
	NOD* avl_succesor(NOD*);
	void avl_print_tree();
	void avl_preordine(NOD*);
	void avl_inordine(NOD*);
	void avl_postordine(NOD*);
	void avl_nivele();
	void avl_construct();
	void avl_empty(NOD*);
	void avl_rot_st(NOD*);
	void avl_rot_dr(NOD*);
	int avl_inaltime_nod(NOD*);
	int avl_factor_balansare_nod(NOD*);
	void avl_transplant(NOD*, NOD*);
	void avl_insert(NOD*);
	void avl_insert_repara(NOD*, bool);
	NOD* avl_delete(NOD*, int);
	void avl_delete_repara(NOD*);
};

void AVL::avl_rot_st(NOD* z) {
	NOD* x;
	x = z->dr;
	z->dr = x->st;
	if (x->st != nullptr)
		x->st->p = z;
	x->p = z->p;
	if (z->p == nullptr)
		rad = x;
	else
	{
		if (z == z->p->st)
			z->p->st = x;
		else
			z->p->dr = x;
	}
	x->st = z;
	z->p = x;
	z->balansare = avl_factor_balansare_nod(z);
	x->balansare = avl_factor_balansare_nod(x);
}

void AVL::avl_rot_dr(NOD* z) {
	NOD* x;
	x = z->st;
	z->st = x->dr;
	if (x->dr != nullptr)
		x->dr->p = z;
	x->p = z->p;
	if (z->p == nullptr)
		rad = x;
	else
	{
		if (x == z->p->dr)
			z->p->dr = x;
		else
			z->p->st = x;
	}
	z->balansare = avl_factor_balansare_nod(z);
	x->balansare = avl_factor_balansare_nod(x);
}

NOD* AVL::avl_caut(int k) {
	NOD* x = rad;
	while (x != nullptr && x->info != k) {
		if (k < x->info)
			x = x->st;
		else
			x = x->dr;
	}
	return x;
}

NOD* AVL::avl_min(NOD* x) {
	NOD* y = x;
	while (y->st != nullptr)
		y = y->st;
	return y;
}

NOD* AVL::avl_max(NOD* x) {
	NOD* y = x;
	while (y->dr != nullptr)
		y = y->dr;
	return y;
}

NOD* AVL::avl_succesor(NOD* x) {
	NOD* y;
	if (x->dr != nullptr) {
		y = avl_min(x->dr);
		return y;
	}
	y = x->p;
	while (y != nullptr && x == y->dr) {
		x = y;
		y = y->p;
	}
	return y;
}

void AVL::avl_preordine(NOD* x) {
	if (x != nullptr) {
		std::cout << x->info << " ";
		avl_preordine(x->st);
		avl_preordine(x->dr);
	}
}

void AVL::avl_inordine(NOD* x) {
	if (x != nullptr) {
		avl_inordine(x->st);
		std::cout << x->info << " " << x->balansare << " ";
		avl_inordine(x->dr);
	}
}

void AVL::avl_postordine(NOD* x) {
	if (x != nullptr) {
		avl_postordine(x->st);
		avl_postordine(x->dr);
		std::cout << x->info << " " << x->balansare << " ";
	}
}

void AVL::avl_nivele() {
	std::queue <const NOD*> coada;
	coada.push(rad);
	while (!coada.empty()) {
		const NOD* x = coada.front();
		coada.pop();
		std::cout << x->info << " " << x->balansare << " ";
		if (x->st != nullptr)
			coada.push(x->st);
		if (x->dr != nullptr)
			coada.push(x->dr);
	}
}

void AVL::avl_print_tree() {
	std::cout << "1. Parcurgerea arborelui in preordine.\n";
	std::cout << "2. Parcurgerea arborelui in inordine.\n";
	std::cout << "3. Pacurgerea arborelui in postordine.\n";
	std::cout << "4. Parcurgerea arborelui pe niveluri.\n";
	int alegere;
	std::cout << "Introduceti alegerea dumneavoastra: ";
	std::cin >> alegere;
	switch (alegere) {
	case 1:
		avl_preordine(rad);
		break;
	case 2:
		avl_inordine(rad);
		break;
	case 3:
		avl_postordine(rad);
		break;
	case 4:
		avl_nivele();
		break;
	default:
		return;
	}
}

void AVL::avl_construct() {
	std::vector<NOD*> vec;
	NOD* nod = nullptr;
	int nr_elem, val;
	std::cout << "Numarul de elemente ale vectorului: ";
	std::cin >> nr_elem;
	for (int i = 0; i < nr_elem; i++) {
		nod = new NOD;
		std::cin >> nod->info;
		vec.push_back(nod);
	}
	for (int i = 0; i < vec.size(); i++)
		avl_insert(vec[i]);
}

void AVL::avl_empty(NOD* radacina) {
	if (radacina == nullptr)
		return;
	else
	{
		avl_empty(radacina->st);
		avl_empty(radacina->dr);
		delete[] radacina;
	}
}

int max(const int a, const int b) {
	return (a > b) ? a : b;
}

int AVL::avl_inaltime_nod(NOD* nod) {
	if (nod == nullptr)
		return 0;
	return max(avl_inaltime_nod(nod->st), avl_inaltime_nod(nod->dr)) + 1;
}

int AVL::avl_factor_balansare_nod(NOD* nod) {
	if (nod == nullptr)
		return 0;
	return avl_inaltime_nod(nod->dr) - avl_inaltime_nod(nod->st);
}

void AVL::avl_transplant(NOD* u, NOD* v) {
	if (u->p != nullptr)
		rad = v;
	else
	{
		if (u == u->p->st)
			u->p->st = v;
		else
			u->p->dr = v;
	}
	if (v != nullptr)
		v->p = u->p;
}

void AVL::avl_insert(NOD* z) {
	NOD* y = nullptr;
	NOD* x = rad;
	bool ok = false;
	while (x != nullptr) {
		y = x;
		if (z->info < x->info)
			x = x->st;
		else
			x = x->dr;
	}
	z->p = y;
	if (y == nullptr)
		rad = z;
	else
	{
		if (z->info < y->info) {
			y->st = z;
			ok = true;
		}
		else
		{
			y->dr = z;
		}
	}
	avl_insert_repara(z->p, ok);
}

void AVL::avl_insert_repara(NOD *nod, bool st) {
	if (nod == nullptr)
		return;
	if (st)
		nod->balansare--;
	else
		nod->balansare++;
	if (nod->balansare == 0)
		return;
	if ((nod->balansare == -1 || nod->balansare == 1) && nod->p != nullptr) {
		if (nod->p->st == nod)
			avl_insert_repara(nod->p, true);
		else
			avl_insert_repara(nod->p, false);
	}
	else
	{
		NOD* aux;
		if (nod->balansare == -2) {
			aux = nod->st;
			if (aux->balansare == -1)
				avl_rot_dr(nod);
			else
			{
				avl_rot_st(aux);
				avl_rot_dr(nod);
			}
		}
		else
			if (nod->balansare == 2) {
				aux = nod->dr;
				if (aux->balansare == 1)
					avl_rot_st(nod);
				else
				{
					avl_rot_dr(aux);
					avl_rot_st(nod);
				}
			}
	}
}

NOD* AVL::avl_delete(NOD *radacina, int x) {
	if (radacina == nullptr)
		return rad;
	if (x < radacina->info)
		radacina->st = avl_delete(radacina->st, x);
	else
		if (x > radacina->info)
			radacina->dr = avl_delete(radacina->dr, x);
		else
		{
			if (radacina->st == nullptr) {
				NOD *aux = radacina->dr;
				delete[] radacina;
				return aux;
			}
			else
				if (radacina->dr == nullptr) {
					NOD* aux = radacina->st;
					delete[] radacina;
					return aux;
				}
			NOD *aux = avl_min(radacina->dr);
			radacina->info = aux->info;
			radacina->dr = avl_delete(radacina->dr, aux->info);
		}
	avl_delete_repara(radacina->p);
	return radacina;
}

void AVL::avl_delete_repara(NOD* nod) {
	if (nod == nullptr)
		return;
	nod->balansare = avl_factor_balansare_nod(nod);

	NOD* aux;
	if (nod->balansare == -2) {
		aux = nod->st;
		if (aux->balansare == 1) {
			avl_rot_st(aux);
			avl_rot_dr(nod);
			avl_delete_repara(nod->p);
		}
		else
			if (aux->balansare == 0) {
				avl_rot_dr(nod);
			}
			else
				if (aux->balansare == -1) {
					avl_rot_dr(nod);
					avl_delete_repara(nod->p);
				}
	}
	else
		if (nod->balansare == 2) {
			aux = nod->dr;
			if (aux->balansare == 1) {
				avl_rot_st(nod);
				avl_delete_repara(nod->p);
			}
			else
				if (aux->balansare == 0) {
					avl_rot_st(nod);
				}
				else
					if (aux->balansare == -1) {
						avl_rot_dr(aux);
						avl_rot_st(nod);
						avl_delete_repara(nod->p);
					}
		}
		else
			avl_delete_repara(nod->p);
}

int main()
{
	AVL arb;
	NOD* nod = nullptr;
	int alegere, val;
	std::cout << "0. EXIT!\n";
	std::cout << "1. Inserati un nod.\n";
	std::cout << "2. Inserati noduri dintr-un vector.\n";
	std::cout << "3. Stergeti un nod.\n";
	std::cout << "4. Stergeti toate nodurile.\n";
	std::cout << "5. Cautati un nod.\n";
	std::cout << "6. Afisarea arborelui.\n";
	std::cout << "7. Afisati minimul.\n";
	std::cout << "8. Afisati maximul.\n";

	while (1) {
		std::cout << "Introduceti una dintre optiunile de mai sus: ";
		std::cin >> alegere;
		switch (alegere) {
		case 0:
			return 0;
		case 1: {
			nod = new NOD;
			std::cout << "Valoarea pe care doriti sa o inserati este: ";
			std::cin >> nod->info;
			arb.avl_insert(nod);
			break;
		}
		case 2: {
			arb.avl_construct();
			break;
		}
		case 3: {
			std::cout << "Valoarea pe care doriti sa o stergeti este: ";
			std::cin >> val;
			nod = new NOD;
			nod = arb.avl_caut(val);
			if (nod == nullptr)
				std::cout << "Nodul nu se afla in arbore!\n";
			else
				arb.rad = arb.avl_delete(arb.rad, nod->info);
			break;
		}
		case 4: {
			arb.avl_empty(arb.rad);
			break;
		}
		case 5: {
			std::cout << "Valoarea pe care doriti sa o cautati este: ";
			std::cin >> val;
			nod = arb.avl_caut(val);
			if (nod == nullptr)
				std::cout << "Nodul nu se afla in arbore!\n";
			else
				std::cout << "Nodul a fost gasit!\n";
		}
		case 6: {
			arb.avl_print_tree();
			break;
		}
		case 7: {
			nod = arb.avl_min(arb.rad);
			std::cout << "Minimul este: " << nod->info << ".\n";
			break;
		}
		case 8: {
			nod = arb.avl_max(arb.rad);
			std::cout << "Maximul este: " << nod->info << ".\n";
			break;
		}
		}
	}
	system("pause");
	return 0;
}